/*
* lab5.c
* Name: Katie Mathews
* User Name: kmathew
* Lecture, Lab Section: 001, 006
* Lab #: Lab #5
* Submitted on: 2/15/2019
* TA: Anurata Hridi
*
* Academic Honesty Declaration:
* The following code represents my own work and I have neither received
nor given assistance that violates the collaboration policy posted with
this assignment. I have not copied or modified code from any other source
other than the lab assignment, course textbook, or course lecture slides.
Any unauthorized collaboration or use of materials not permitted will be
subjected to academic integrity policies of Clemson University and CPSC
1010/1011.
*
* I acknowledge that this lab assignment is based upon an assignment
created by Clemson University and that any publishing or posting of this
code is prohibited unless I receive written permission from Clemson
University.
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;

//enum to store the suit values
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//struct to store the suit and value of a specific card
typedef struct Card {
  Suit suit;
  int value;
} Card;

//function declarations
string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i; }

int main(int argc, char const *argv[]) {
	// IMPLEMENT as instructed below
	/*This is to seed the random generator */
	srand(unsigned (time(0)));

 	/*Create a deck of cards of size 52 (hint this should be an array) and
  	*initialize the deck*/
	Card deckofCards[52];

	//looping through the array of cards
	for (int i = 0; i < 52; i++)
	{
		//modding the value of the card from 2-14 and setting that value to i 
		deckofCards[i].value = (((i % 13) + 2));
		//modding the value of the suit (need to type cast i)
		deckofCards[i].suit = static_cast<Suit>((i % 4));
	}

  	/*After the deck is created and initialzed we call random_shuffle() see the
   	*notes to determine the parameters to pass in.*/

	//shuffling through the deck from the first element to 1 past the last element using the sorting
	//function myrandom
	random_shuffle(&deckofCards[0], &deckofCards[52], myrandom);


	/*Build a hand of 5 cards from the first five cards of the deck created
	*above*/
	Card handofCards[5] = {deckofCards[0], deckofCards[1], deckofCards[2], deckofCards[3], 
	deckofCards[4]};

	/*Sort the cards.  Links to how to call this function is in the specs
    	*provided*/

	//sorting through the hand from the first element to 1 past the last element using the sorting 
	//function suit_order
	sort(&handofCards[0], &handofCards[5], suit_order);
	
    	/*Now print the hand below. You will use the functions get_card_name and
     	*get_suit_code */

	//looping through the hand of cards 
	for (int i = 0; i < 5; i++)
	{
		//printing the value and the suit, right justified with a width of 10
		cout << setw(10) << right << get_card_name(handofCards[i]) << " of " << 
		get_suit_code(handofCards[i]) << right << endl;
	}

	return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

//this function takes a card and determines and returns the suit value
bool suit_order(const Card& lhs, const Card& rhs) 
{	
	if (lhs.suit < rhs.suit)
	{
		return true;
	}
	
	if (lhs.suit > rhs.suit)
	{
		return false;
	}

	else 
	{
		if (lhs.value < rhs.value)
		{	
			return true;
		}
		
		else
		{
			return false;
		}
		
	}
}

//this function takes in a specifc card's suit and determines and returns the symbol for that suit
string get_suit_code(Card& c) 
{
	switch (c.suit) {
	
	case SPADES:    return "\u2660";
	case HEARTS:    return "\u2661";
	case DIAMONDS:  return "\u2662";
    	case CLUBS:     return "\u2663";
    	default:        return "";
  	}
}

//this function takes in a specific card's value and determines and returns the value for that card
string get_card_name(Card& c) 
{
	switch (c.value) {

	case 2:		return "2";
	case 3:		return "3";
	case 4:		return "4";
	case 5:		return "5";
	case 6:		return "6";
	case 7:		return "7";
	case 8:		return "8";
	case 9:		return "9";
	case 10:	return "10";
	case 11:	return "Jack";
	case 12:	return "Queen";
	case 13:	return "King"; 
	case 14:	return "Ace"; 
	default:        return "";
	}

}
